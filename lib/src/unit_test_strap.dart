/// This function is intended to wrap your Unit tests so that you can access the
/// [UnitTestGiven], [UnitTestWhen], and [UnitTestWhen] to compose the test
/// case. You will want to create some of your own test support code that is
/// specific to your code under test. It should look something like this

typedef UnitTestStrapCallback<T> = Future<void> Function(
    UnitTestGiven<T>, UnitTestWhen<T>, UnitTestThen<T>);

Future<void> givenWhenThenUnitTest<T>(
        T strap, UnitTestStrapCallback<T> callback) =>
    callback(UnitTestGiven(strap), UnitTestWhen(strap), UnitTestThen(strap));

/// You can use this class directly and then author your own extensions to it
/// for your specific behaviors or you can extend this class and provide any
/// dependencies you require.
class UnitTestGiven<T> {
  UnitTestGiven(this.strap);

  final T strap;
}

class UnitTestWhen<T> {
  UnitTestWhen(this.strap);

  final T strap;
}

class UnitTestThen<T> {
  UnitTestThen(this.strap);

  final T strap;
}
