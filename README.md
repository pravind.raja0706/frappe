# Frappe 🧋

A Flutter package for creating more readable tests. If you are not familiar with 
[Flutter's Widget tests](https://flutter.dev/docs/cookbook/testing/widget/introduction) please take a moment to review them.

**Given** I feel that this test package is less verbose and much easier to use  
**When** one reads any given test.  
**Then** it should be easy to understand.

The intention of this package is to enable code reuse while making our tests more readable. 
Consider this very simple widget test case 
from [Flutter's Widget tests documentation](https://flutter.dev/docs/cookbook/testing/widget/introduction).

### Without Frappe
```dart
testWidgets('MyWidget has a title and message', (WidgetTester tester) async {
    await tester.pumpWidget(MyWidget(title: 'T', message: 'M'));
    final titleFinder = find.text('T');
    final messageFinder = find.text('M');

    // Use the `findsOneWidget` matcher provided by flutter_test to verify
    // that the Text widgets appear exactly once in the widget tree.
    expect(titleFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
});
```

### With Frappe
```dart
testWidgets('MyWidget has a title and message', strap((given, when, then) async {
    await given.pumpMyWidget(title: 'T', message: 'M');
    then.myTitleIs('T');
    then.myMessageIs('M');
  }));
```

As you can see, we are able to remove the details that are not important by moving them into the classes [WidgetTestGiven](lib/src/widget_test_strap.dart), [WidgetTestWhen](lib/src/widget_test_strap.dart), and [WidgetTestThen](lib/src/widget_test_strap.dart).

For a complete example of how to use this please look at sample tests here: [example_widget_test_support.dart](test/example_widget_test_support.dart)....