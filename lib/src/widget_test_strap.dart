import 'package:flutter_test/flutter_test.dart';

import './unit_test_strap.dart';

typedef WidgetTestStrapCallback<T extends WidgetTestStrap> = Future<void>
    Function(WidgetTestGiven<T>, WidgetTestWhen<T>, WidgetTestThen<T>);

Future<void> givenWhenThenWidgetTest<T extends WidgetTestStrap>(
        T strap, WidgetTestStrapCallback<T> callback) =>
    callback(
        WidgetTestGiven(strap), WidgetTestWhen(strap), WidgetTestThen(strap));

class WidgetTestStrap {
  const WidgetTestStrap(this.tester);
  final WidgetTester tester;
}

class WidgetTestGiven<T extends WidgetTestStrap> extends UnitTestGiven<T> {
  WidgetTestGiven(T strap) : super(strap);

  WidgetTester get tester => strap.tester;
}

class WidgetTestWhen<T extends WidgetTestStrap> extends UnitTestWhen<T> {
  WidgetTestWhen(T strap) : super(strap);

  WidgetTester get tester => strap.tester;
}

class WidgetTestThen<T extends WidgetTestStrap> extends UnitTestThen<T> {
  WidgetTestThen(T strap) : super(strap);

  WidgetTester get tester => strap.tester;
}
