import 'package:flutter_test/flutter_test.dart';
import 'package:frappe/frappe.dart';

Future<void> Function() strap(
    UnitTestStrapCallback<_ExampleUnitTestStrap> callback) {
  return () => givenWhenThenUnitTest(_ExampleUnitTestStrap(), callback);
}

class _ExampleUnitTestStrap {
  _ExampleUnitTestStrap() : super();

  int counter = 0;
}

extension ExampleGiven on UnitTestGiven<_ExampleUnitTestStrap> {
  void preCondition() {
    this.strap.counter = 1;
  }
}

extension ExampleWhen on UnitTestWhen<_ExampleUnitTestStrap> {
  Future<void> userPerformsSomeAction() async {
    this.strap.counter++;
  }
}

extension ExampleThen on UnitTestThen<_ExampleUnitTestStrap> {
  void makeSomeAssertion() {
    expect(this.strap.counter, equals(2));
  }
}
