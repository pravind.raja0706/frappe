import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:frappe/frappe.dart';

Future<void> Function(WidgetTester) strap(
    WidgetTestStrapCallback<_ExampleWidgetTestStrap> callback) {
  return (tester) =>
      givenWhenThenWidgetTest(_ExampleWidgetTestStrap(tester), callback);
}

class _ExampleWidgetTestStrap extends WidgetTestStrap {
  _ExampleWidgetTestStrap(WidgetTester tester) : super(tester);
}

extension ExampleGiven on WidgetTestGiven<_ExampleWidgetTestStrap> {
  Future<void> preCondition() async {
    await tester.pumpWidget(WidgetUnderTest());
  }
}

extension ExampleWhen on WidgetTestWhen<_ExampleWidgetTestStrap> {
  Future<void> userPerformsSomeAction() async {
    await tester.tap(find.text('0'));
  }
}

extension ExampleThen on WidgetTestThen<_ExampleWidgetTestStrap> {
  Future<void> makeSomeAssertion() async {
    await tester.pump();
    expect(find.text('1'), findsOneWidget);
  }
}

class WidgetUnderTest extends StatefulWidget {
  @override
  _WidgetUnderTestState createState() => _WidgetUnderTestState();
}

class _WidgetUnderTestState extends State<WidgetUnderTest> {
  int _counter = 0;
  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.ltr,
        child: GestureDetector(
          onTap: () {
            setState(() {
              _counter++;
            });
          },
          child: Text(_counter.toString()),
        ));
  }
}
